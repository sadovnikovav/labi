﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebApplication1.ServiceReference1;

namespace WebApplication1.Services
{
    public class ProductService
    {
        public async Task<IEnumerable<Customer>> GetCustomersAsync()
        {
            using (var productService = new ProductServiceClient())
            {
                var customers = await productService.GetCustomersAsync();

                return customers;
            }
        }

        public async Task<IEnumerable<Product>> GetProductsAsync(int customerID)
        {
            using (var productService = new ProductServiceClient())
            {
                var products = await productService.GetProductsByCustomerIDAsync(customerID);

                return products;
            }
        }
    }
}