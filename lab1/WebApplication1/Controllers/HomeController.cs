﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using WebApplication1.Models;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ProductService _productService;

        public HomeController()
        {
            _productService = new ProductService();
        }

        public async Task<ActionResult> Index()
        {
            var customers = await _productService.GetCustomersAsync();

            var mainViewModel = new MainViewModel
            {
                Customers = customers
            };

            return View(mainViewModel);
        }

        [HttpGet]
        public async Task<string> Customers()
        {
            var customers = await _productService.GetCustomersAsync();
            
            return JsonConvert.SerializeObject(customers);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<string> Products(int id)
        {
            var products = await _productService.GetProductsAsync(id);

            return JsonConvert.SerializeObject(products);
        }
    }
}