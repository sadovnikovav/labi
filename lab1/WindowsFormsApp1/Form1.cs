﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.ServiceReference1;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var selectedCells = customersDataGridView.SelectedCells;
            if (selectedCells.Count < 1)
            {
                MessageBox.Show("Выберите клиента");
                return;
            }

            var customer = selectedCells[0].OwningRow?.DataBoundItem as Customer;
            if (customer == null)
            {
                return;
            }

            IEnumerable<Product> products;
            using (var client = new ProductServiceClient())
            {
                products = client.GetProductsByCustomerID(customer.CustomerID);
            }

            productsDataGridView.DataSource = products;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            customersDataGridView.MultiSelect = false;

            var customers = GetCustomers();
            customersDataGridView.DataSource = customers;
        }

        private void refreshCustomersButton_Click(object sender, EventArgs e)
        {
            var customers = GetCustomers();
            customersDataGridView.DataSource = customers;
        }

        private static IEnumerable<Customer> GetCustomers()
        {
            IEnumerable<Customer> customers;
            using (var productService = new ProductServiceClient())
            {
                customers = productService.GetCustomers();
            }

            return customers;
        }
    }
}
