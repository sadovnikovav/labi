﻿using System.Collections.Generic;
using System.ServiceModel;

namespace WcfService1
{
    [ServiceContract]
    public interface IProductService
    {
        [OperationContract]
        CustomerData GetByCustomerID(int customerID);

        [OperationContract]
        IEnumerable<Logic.Customer> GetCustomers();

        [OperationContract]
        IEnumerable<Logic.Product> GetProductsByCustomerID(int customerID);
    }
}