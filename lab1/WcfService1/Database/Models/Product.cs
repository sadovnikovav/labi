﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WcfService1.Database.Models
{
    public class Product
    {
        public int ProductID { get; set; }
        [ForeignKey(nameof(Customer))]
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public Customer Customer { get; set; }
    }
}