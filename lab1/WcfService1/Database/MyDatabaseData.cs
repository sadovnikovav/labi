﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WcfService1.Database.Models;

namespace WcfService1.Database
{
    public class MyDatabaseData : DbContext
    {
        public MyDatabaseData() : base("MyDatabaseDataConnectionString")
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}