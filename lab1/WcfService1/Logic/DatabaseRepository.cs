﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

using WcfService1.Database;

namespace WcfService1.Logic
{
    public class DatabaseRepository
    {
        public async Task<CustomerData> GetByCustomerIDAsync(int customerID)
        {
            using (var db = new MyDatabaseData())
            {
                var pd = await db.Customers
                    .Include(x => x.Products)
                    .FirstOrDefaultAsync(x => x.ID == customerID);

                var result = new CustomerData
                {
                    CustomerID = pd.ID,
                    CustomerName = pd.Name,
                    Products = pd.Products.Select(x => new Logic.Product
                    {
                        ProductID = x.ProductID,
                        CustomerID = x.CustomerID,
                        Name = x.Name,
                        Price = x.Price
                    })
                };

                return result;
            }
        }

        public async Task<IEnumerable<Logic.Customer>> GetCustomersAsync()
        {
            using (var db = new MyDatabaseData())
            {
                var pd = await db.Customers.Select(x => new Logic.Customer
                {
                    CustomerID = x.ID,
                    Name = x.Name
                }).ToListAsync();

                return pd;
            }
        }

        public async Task<IEnumerable<Logic.Product>> GetProductsByCustomerAsync(int customerID)
        {
            using (var db = new MyDatabaseData())
            {
                var products = await db.Products
                    .Where(x => x.CustomerID == customerID)
                    .Select(x => new Logic.Product
                    {
                        ProductID = x.ProductID,
                        CustomerID = x.CustomerID,
                        Name = x.Name,
                        Price = x.Price
                    }).ToListAsync();

                return products;
            }
        }
    }
}