﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WcfService1
{
    [DataContract]
    public class CustomerData
    {
        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public IEnumerable<Logic.Product> Products { get; set; }

    }
}