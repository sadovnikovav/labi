namespace WcfService1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerProduct : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employees", "CustomerID", "dbo.Customers");
            DropIndex("dbo.Employees", new[] { "CustomerID" });
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        CustomerID = c.Int(nullable: false),
                        Name = c.String(),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ProductID)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .Index(t => t.CustomerID);
            
            DropTable("dbo.Employees");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CustomerID = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.Products", "CustomerID", "dbo.Customers");
            DropIndex("dbo.Products", new[] { "CustomerID" });
            DropTable("dbo.Products");
            CreateIndex("dbo.Employees", "CustomerID");
            AddForeignKey("dbo.Employees", "CustomerID", "dbo.Customers", "ID", cascadeDelete: true);
        }
    }
}
