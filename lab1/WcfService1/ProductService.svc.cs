﻿using System.Collections.Generic;
using WcfService1.Logic;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ProductService : IProductService
    {
        public CustomerData GetByCustomerID(int customerID)
        {
            var repository = new DatabaseRepository();
            var customer = repository.GetByCustomerIDAsync(customerID).Result;
            return customer;
        }

        public IEnumerable<Logic.Customer> GetCustomers()
        {
            var repository = new DatabaseRepository();
            var customers = repository.GetCustomersAsync().Result;
            return customers;
        }

        public IEnumerable<Logic.Product> GetProductsByCustomerID(int customerID)
        {
            var repository = new DatabaseRepository();
            var products = repository.GetProductsByCustomerAsync(customerID).Result;
            return products;
        }
    }
}
